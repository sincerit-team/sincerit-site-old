﻿$(document).ready(function () {
    $(".ui-loader").hide();
    if (navigator.appVersion.indexOf("Chrome/") != -1)
        $("#quota").css("font-size", "14px");

    $("a").removeClass("selected");
    if (window.location.pathname.indexOf("ru", 0) > -1)
        $($("a")[2]).addClass("selected");
    else if (window.location.pathname.indexOf("de", 0) > -1)
        $($("a")[1]).addClass("selected");
    else
        $($("a")[0]).addClass("selected");

    var customers = $("#customers");
    var projects = $("#projects");
    $("img", customers).bind("click", function (event) {
        showTooltip(event);
    });
    $("img", projects).bind("click", function (event) {
        showTooltip(event);
    });

    $(document).bind("tap", hidePopupsTouch);

    setTimeout(function () { $("#requestquote").find("textarea").trigger('autosize.destroy'); }, 400);
});

function hidePopups(event) {
    if (!navigator.userAgent.match(/iPhone/i)) {
        if ($("#requestquote").is(":visible")) {
            var el1 = document.elementFromPoint(event.clientX, event.clientY);
            if ($(el1).offsetParent().attr("id") != "requestquote")
                hideRequest();
        }
    }

    if ($("#tooltip").is(":visible"))
        hideTooltip();
}

function hidePopupsTouch(event) {
    //if (!navigator.userAgent.match(/iPhone/i)) 
    {
        if ($("#requestquote").is(":visible")) {
            var el1 = document.elementFromPoint(event.clientX, event.clientY);
            //alert("id:" + $(el1).attr("id") + ";" + event.pageX + ':' + event.pageY + ";" + event.clientX + ":" + event.clientY);
            if ($(el1).offsetParent().attr("id") != "requestquote")
                hideRequest();
        }
    }

    if ($("#tooltip").is(":visible"))
        hideTooltip();
}

//$(document).click(function (event) {
//    //hidePopups(event);
//});

function clickMore(index) {
    if ($("#head" + index).hasClass("selected")) {
        hideMore();
        return;
    }

    $("#content").hide();

    $("#article1").hide();
    $("#article2").hide();
    $("#article3").hide();

    $('div[id^="shape"]').hide();
    $('div[id^="head"]').removeClass("selected");

    $("#shape" + index).show();

    $(".more_col").css("display", "table");

    $("#head" + index).addClass("selected");
    $("#article" + index).show();
}

function hideMore() {
    $('div[id^="head"]').removeClass("selected");
    $("#content").show();
    $(".more_col").css("display", "none");
}

function showRequest(event) {
    var tmpEvent = event || window.event;

    $("#shadow").show();
    $("#requestquote").find("input").show();
    $("#requestquote").find("input").removeClass("valid");
    $("#requestquote").find("input").removeClass("invalid");
    $("#requestquote").find("textarea").show();
    $("#requestquote").find("textarea").removeClass("valid");
    $("#requestquote").find("textarea").removeClass("invalid");
    $(".request_content").find("span").show();
    $("#requestquote").find(".completionMessage").css("display", "none");

    $("#requestquote").show();
    $("#requestquote").center();

    tmpEvent.preventDefault();
    tmpEvent.stopPropagation();
}

function hideRequest() {
    $("#shadow").hide();
    $("#requestquote").find("input").val('');
    $("#requestquote").find("textarea").val('');
    $("#requestButton").addClass("disabled");
    $("#requestquote").hide();
}

function showTooltip(event) {
    var tmpEvent = event || window.event;
    var heightOffset = $(event.currentTarget).position().top;
    var widthOffset = event.currentTarget.offsetLeft + event.currentTarget.offsetWidth / 2 - 190 + "px";
    var mq = window.matchMedia("(max-device-width: 480px)");
    if (mq.matches)
        widthOffset = 20;

    var tooltip = $(event.currentTarget).attr("tooltip");
    if (tooltip == "no")
        return;
    var id = $(event.currentTarget).attr("content");
    var content = $("#" + id).html();
    $(".tooltip_content").html(content);

    var height = $("#tooltip").height();
    $("#tooltip").css({
        "top": -(height - heightOffset) + "px",
        "left": widthOffset,
    });
    $("#tooltip").show();
    tmpEvent.preventDefault();
    tmpEvent.stopPropagation();
}

function hideTooltip(event) {
    $("#tooltip").hide();
}

function requestQuota() {
    if (!validateControls()) {
        var data = { Name: $("#name").val(), Company: $("#company").val(), EMail: $("#email").val(), description: $("#decription").val() };
        var str = JSON.stringify(data);
        var url = "ru/home/requestaquota";
        $.ajax(url, {
            data: str,
            type: "post", contentType: 'application/json',
            success: function (result) {
                $("#requestquote").find("input").hide();
                $("#requestquote").find("textarea").hide();
                $(".request_content").find("span").hide();
                $("#requestquote").find(".completionMessage").css("display", "inline-block");
            },
            error: function (result) {
                $("#requestquote").find("input").hide();
                $("#requestquote").find("textarea").hide();
                $(".request_content").find("span").hide();
                $("#requestquote").find(".completionMessage").css("display", "inline-block");
            }
        });
    };
}

jQuery.fn.center = function ($) {
    var w = jQuery(window);
    //this.css({
    //    'position': 'fixed',
    //    'top': Math.abs(((w.height() - this.outerHeight()) / 2) + w.scrollTop()),
    //    'left': Math.abs(((w.width() - this.outerWidth()) / 2) + w.scrollLeft())
    //});
    return this;
}

function validateControls(el) {
    if (el != null) {
        $(el).removeClass("invalid");
        $(el).removeClass("valid");

        var isValid = !($(el).val() == '');
        if (el.id == "email" && isValid)
            isValid = validateEmail($(el).val());

        $(el).addClass(isValid ? "valid" : "invalid");
    }

    var isEmpty = false;
    if ($("#name").val() == '')
        isEmpty = true;
    if ($("#company").val() == '')
        isEmpty = true;
    if ($("#email").val() == '')
        isEmpty = true;
    else
        if (!isEmpty)
            isEmpty = !validateEmail($("#email").val());
    if ($("#decription").val() == '')
        isEmpty = true;

    $("#requestButton").removeClass("disabled");
    if (isEmpty) {
        $("#requestButton").addClass("disabled");
    }

    return isEmpty;
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}