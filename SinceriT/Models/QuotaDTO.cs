﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SinceriT.Models
{
    public class QuotaDTO
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string EMail { get; set; }
        public string Description { get; set; }
    }
}