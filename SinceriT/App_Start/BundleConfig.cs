﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Optimization;

namespace SinceriT
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/jquery-ui-{version}.js",
                "~/Scripts/jquery.mobile-{version}.js"));
            bundles.Add(new ScriptBundle("~/bundles/script").Include(
                "~/Scripts/script.js"));
        }
    }
}
