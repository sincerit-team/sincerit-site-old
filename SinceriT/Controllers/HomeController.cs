﻿using SinceriT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;


namespace SinceriT.Controllers
{
    public class HomeController : Controller
    {
        private SincerITEntities db = new SincerITEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ChangeCulture(string lang)
        {
            var langCookie = new HttpCookie("lang", lang) { HttpOnly = true };
            Response.AppendCookie(langCookie);
            return RedirectToAction("Index", "Home", new { culture = lang });
        }

        public void RequestAQuota([FromBody]QuotaDTO quota)
        {
            SendMail(quota.Name, quota.Company, quota.EMail, quota.Description);

            db.RequestQuotas.Add(new RequestQuota()
                {
                    ID = Guid.NewGuid(),
                    Name = quota.Name,
                    Company = quota.Company,
                    EMail = quota.EMail,
                    Description = quota.Description
                });
            db.SaveChangesAsync();
        }

        public void SendMail(string name, string company, string email, string description)
        {
            new Thread(() =>
            {
                try
                {
                    MailMessage mm = new MailMessage("tech@sincerit.com", "info@sincerit.com");
                    mm.Subject = "Request a quote";
                    mm.Body = string.Format("Name:\t\t{0}\r\nCompany:\t{1}\r\nEMail:\t\t{2}\r\nDescription:\t{3}", name, company, email, description);
                    mm.IsBodyHtml = false;

                    SmtpClient sc = new SmtpClient("smtp.yandex.ru", 25);
                    sc.EnableSsl = true;
                    sc.DeliveryMethod = SmtpDeliveryMethod.Network;
                    sc.UseDefaultCredentials = false;
                    sc.Credentials = new NetworkCredential("tech@sincerit.com", "sin_tech14");
                    sc.Send(mm);
                }
                catch (Exception ex)
                {
                    string e = ex.Message;
                }
            }).Start();
        }

        public async Task SendMailOld(string name, string company, string email, string description)
        {
            //Task task = null;

            try
            {
                MailMessage mm = new MailMessage("tech@sincerit.com", "info@sincerit.com");
                mm.Subject = "Request a quote";
                mm.Body = string.Format("Name:\t\t{0}\r\nCompany:\t{1}\r\nEMail:\t\t{2}\r\nDescription:\t{3}", name, company, email, description);
                mm.IsBodyHtml = false;

                SmtpClient sc = new SmtpClient("smtp.yandex.ru", 25);
                sc.EnableSsl = true;
                sc.DeliveryMethod = SmtpDeliveryMethod.Network;
                sc.UseDefaultCredentials = false;
                sc.Credentials = new NetworkCredential("tech@sincerit.com", "sin_tech14");
                await sc.SendMailAsync(mm);
            }
            catch (Exception ex)
            {
                string e = ex.Message;
            }

            //return task;
        }
    }
}
